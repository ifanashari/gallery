import Vue from 'vue'
import App from './App'
import router from './router'
import jQuery from 'jQuery'
global.jQuery = jQuery
global.$ = jQuery

import './assets/css/navigation.css'
import './assets/css/sections.css'
import './assets/css/home.css'
// import './assets/js/owl.js'


Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
